var gulp = require('gulp')
//var autoprefixer = require('gulp-autoprefixer')
var autocss = require('gulp-autocss')
var concat = require('gulp-concat')
var browserSync = require('browser-sync')

gulp.task('autocss', function() {
    return gulp.src(['css/*.css', 'index.html'])
        .pipe(autocss())
        //.pipe(autoprefixer())
        .pipe(concat('build.css'))
        .pipe(gulp.dest('.'));
});

gulp.task('serve', ['autocss'], function(done) {
  browserSync({
    open: false,
    files: [
      'build/**/*.css',
      '*.html',
    ],
    server: {
      baseDir: ['.'],
    },
  }, done);
})

gulp.task('watch', ['serve'], function() {
  gulp.watch('*.html', ['autocss'])
})

gulp.task('default', ['autocss', 'watch'])
