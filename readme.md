
# AutoCSS Demos

## index.html
Basic demo using gulp-autocss, classnames are parsed from index.html and sent to autocss and postcss-cssnext which finally generates build.css.

## client.html
Client side CSS-generation, this will only work in browsers supporting CSS-variables.

# TODO
- Postcss-cssnext
- Autoprefixer
